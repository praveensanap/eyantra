/*
 * IncFile2.h
 *
 * Created: 3/2/2014 2:55:24 AM
 *  Author: Harshit
 */ 


#ifndef INCFILE2_H_
#define INCFILE2_H_

unsigned char sharp;
unsigned int value;
void forcentreleft(void)
{
	int f=0;
	while(f !=1)
	{
		//Left_white_line = ADC_Conversion(3);	//Getting data of Left WL Sensor
		Center_white_line = ADC_Conversion(2);	//Getting data of Center WL Sensor
		//Right_white_line = ADC_Conversion(1);	//Getting data of Right WL Sensor
		//print_sensor(1,1,3);	//Prints value of White Line Sensor1
		print_sensor(1,5,2);	//Prints Value of White Line Sensor2
		//6print_sensor(1,9,1);	//Prints Value of White Line Sensor3
		if (Center_white_line> 10)
		{f=1;
		}
	}
	if(ShaftCountLeft>=27 && ShaftCountLeft<=30 ||ShaftCountRight>=27 && ShaftCountRight<=30)
	{right();
		velocity(255,100);
		_delay_ms(150);
		stop();
		
	}
	else if(ShaftCountLeft>=31 && ShaftCountLeft<=34 ||ShaftCountRight>=31 && ShaftCountRight<=34)
	{right();
		velocity(255,100);
		_delay_ms(250);
		stop();
		
	}
	else if(ShaftCountLeft>34 && ShaftCountLeft<=36 ||ShaftCountRight>34 && ShaftCountRight<=36)
	{right();
		velocity(255,100);
		_delay_ms(350);
		stop();
		
	}
	else {
		right();
		_delay_ms(150);
		stop();
	}
}
void left1(void)
{
	Center_white_line = 0;
	left();
	velocity(210,210);
	_delay_ms(500);
	forcentreleft();
}
void forcentreright(void)
{
	int f=0;
	while(f !=1)
	{
		//Left_white_line = ADC_Conversion(3);	//Getting data of Left WL Sensor
		Center_white_line = ADC_Conversion(2);	//Getting data of Center WL Sensor
		//Right_white_line = ADC_Conversion(1);	//Getting data of Right WL Sensor
		//print_sensor(1,1,3);	//Prints value of White Line Sensor1
		print_sensor(1,5,2);	//Prints Value of White Line Sensor2
		//6print_sensor(1,9,1);	//Prints Value of White Line Sensor3
		if (Center_white_line> 20)
	{f=1;	}
	}
	if(ShaftCountLeft>=28 && ShaftCountLeft<=30||ShaftCountRight>=28 && ShaftCountRight<=30)
	{left();
		velocity(100,255);
		_delay_ms(150);
		stop();
		
	}
	else if(ShaftCountLeft>=31&& ShaftCountLeft<=34||ShaftCountRight>=31 && ShaftCountRight<=34)
	{left();
		velocity(100,255);
		_delay_ms(250);
		stop();
		
	}

	else if(ShaftCountLeft>34 && ShaftCountLeft<=36||ShaftCountRight>34 && ShaftCountRight<=36)
	{left();
		velocity(100,255);
		_delay_ms(350);
		stop();
		
	}
	else {
		left();
		_delay_ms(150);
		stop();
	}
}


void right1(void)
{
	ShaftCountLeft=0;
	ShaftCountLeft=0;
	Center_white_line = 0;
	right();
	velocity(210,210);
	_delay_ms(500);
	forcentreright();
}


void backward1(unsigned long int distance)
{
	int f=0;
	int g=0;
	int h=0;
	float actualdshaft=distance/5.338;
	ShaftCountLeft=0;
	ShaftCountRight=0;
	volatile unsigned long int d=0;
	while( d<actualdshaft)
	{
		d=(ShaftCountLeft+ShaftCountRight)/2;

		Left_white_line = ADC_Conversion(3);	//Getting data of Left WL Sensor
		Center_white_line = ADC_Conversion(2);	//Getting data of Center WL Sensor
		Right_white_line = ADC_Conversion(1);	//Getting data of Right WL Sensor
		print_sensor(1,1,3);	//Prints value of White Line Sensor1
		print_sensor(1,5,2);	//Prints Value of White Line Sensor2
		print_sensor(1,9,1);	//Prints Value of White Line Sensor3
		
		if (Center_white_line>28 || f==1)
		{
			
			back();
			velocity(245,245);
			f=0;
			h=0;
			g=0;
		}
		if(Center_white_line<28 && g==0)
		{
			back();
			velocity(220,240);
			if(Center_white_line>10)
			{
				f=1;
			}
			if(Left_white_line>10)
			{
				g=1;
			}
			
		}
		if(Center_white_line<28 && g==1 )
		{
			back();
			velocity(240,220);
			if (Center_white_line>10)
			{h=1;
			}
		}
		if (Center_white_line<28 && h==1)
		{
			back();
			velocity(220,240);
		}
	}
	
	stop();
}

void forward1(unsigned long int distance)
{
	int f=0;
	int g=0;
	int h=0;
	unsigned long int actualdshaftint;
	float actualdshaft=distance/5.338;
	actualdshaftint=(unsigned long int)actualdshaft;
	ShaftCountRight=0;
	ShaftCountLeft=0;
	float d=0;
	while( d<actualdshaftint)7
	{
		d=(ShaftCountLeft+ShaftCountRight)/2;
		Left_white_line = ADC_Conversion(3);	//Getting data of Left WL Sensor
		Center_white_line = ADC_Conversion(2);	//Getting data of Center WL Sensor
		Right_white_line = ADC_Conversion(1);	//Getting data of Right WL Sensor
		print_sensor(1,1,3);	//Prints value of White Line Sensor1
		print_sensor(1,5,2);	//Prints Value of White Line Sensor2
		print_sensor(1,9,1);	//Prints Value of White Line Sensor3
		
		if (Center_white_line>28)
		{
			
			forward();
			velocity(235,235);
			//f=0;
			//h=0;
			g=0;
			
		}
		
		
		
		if(Center_white_line<28 && g==0)
		{
			forward();
			velocity(210,230);
			Left_white_line = ADC_Conversion(3);	//Getting data of Left WL Sensor
			Center_white_line = ADC_Conversion(2);	//Getting data of Center WL Sensor
			Right_white_line = ADC_Conversion(1);	//Getting data of Right WL Sensor
			print_sensor(1,1,3);	//Prints value of White Line Sensor1
			print_sensor(1,5,2);	//Prints Value of White Line Sensor2
			print_sensor(1,9,1);	//Prints Value of White Line Sensor3
			if(Center_white_line>10)
			{
				forward();
				velocity(220,210);
			}
			
			if(Right_white_line>10)
			{
				g=1;
			}
			
		}
		


		
		if(Center_white_line<28 && g==1 )
		{
			forward();
			velocity(220,180);
			Left_white_line = ADC_Conversion(3);	//Getting data of Left WL Sensor
			Center_white_line = ADC_Conversion(2);	//Getting data of Center WL Sensor
			Right_white_line = ADC_Conversion(1);	//Getting data of Right WL Sensor
			print_sensor(1,1,3);	//Prints value of White Line Sensor1
			print_sensor(1,5,2);	//Prints Value of White Line Sensor2
			print_sensor(1,9,1);	//Prints Value of White Line Sensor3
			
			if (Center_white_line>10)
			{
				forward();
				velocity(210,230);
			}
			
		}
		
	}
	
	
	
	stop();
}



void servo1(void)
{
	unsigned char s1 = 0;
	int c=0;
	while(c<3)
	{
		for(s1=0;s1<30;s1++)
		{
			servo_1(s1);
			_delay_ms(12);
		}
		c++;
	}
}
void checksharp(void)
{
	value=900;
	while (value>120)
	{

		sharp = ADC_Conversion(11);						//Stores the Analog value of front sharp connected to ADC channel 11 into variable "sharp"
		value = Sharp_GP2D12_estimation(sharp);				//Stores Distance calsulated in a variable "value".
		lcd_print(2,14,value,3); 						//Prints Value Of Distanc in MM measured by Sharp Sensor.
	}
	stop();
}

void turnleftdrop(void)
{
	left();
	velocity(210,210);
	checksharp();
	servo1();
}

void turnrightdrop(void)
{
	right();
	velocity(210,210);
	checksharp();
	servo1();
}


#endif /* INCFILE2_H_ */