/*
 * Astar.h
 *
 * Created: 2/28/2014 12:09:42 PM
 *  Author: Praveen Sanap
 * Author:  Sairaj Desai
 */ 


#ifndef ASTAR_H_
#define ASTAR_H_

void move(int pos[3], int mov, int dir){
  int x = pos[0];
  int y = pos[1];
  int theta = pos[2];
  int c = theta + mov;
  int x1=x;
  int y1 = y;
  int theta1 = theta;
  switch(c){
  case 1:
    y1 = y+dir;
    break;
  case 91:
    x1 = x-dir;
    break;
  case 181:
    y1 = y-dir;
    break;
  case 271:
    x1 = x+dir;
    break;
  case 2:
    y1 = y-dir;
    break;
  case 92:
    x1 = x+dir;
    break;
  case 182:
    y1 = y+dir;
    break;
  case 272:
    x1 = x-dir;
    break;
  case 3:
    if (dir==1){
      x1 = x-1;
    }else if(dir==-1){
      y1 = y-1;
    }
    theta1 = dir==1?(theta+90)%360:(theta+270)%360;
    break;
  case 93:
    if (dir==1){
      y1 = y-dir;
    }else if(dir==-1){
      x1=x+1;
    }
    theta1 = dir==1?(theta+90)%360:(theta+270)%360;
    break;
  case 183:
    if (dir==1){
      x1 = x+dir;
    }else if(dir==-1){
      y1 = y+1;
    }
    theta1 = dir==1?(theta+90)%360:(theta+270)%360;
    break;
  case 273:
    if (dir==1){
      y1 = y+dir;
    }else if(dir==-1){
      x1 = x-1;
    }
    theta1 = dir==1?(theta+90)%360:(theta+270)%360;
    break;
  case 4:
    if (dir==1){
      x1 = x+dir;
    }else if(dir==-1){
      y1 = y-1;
    }
    theta1 = dir==1?(theta+270)%360:(theta+90)%360;
    break;
  case 94:
    if (dir==1){
      y1 = y+dir;
    }else if(dir==-1){
      x1 = x+1;
    }
    theta1 = dir==1?(theta+270)%360:(theta+90)%360;
    break;
  case 184:
    if (dir==1){
      x1 = x-1;
    }else if(dir==-1){
      y1 = y+1;
    }
    theta1 = dir==1?(theta+270)%360:(theta+90)%360;
    break;
  case 274:
    if (dir==1){
      y1 = y-1;
    }else if(dir==-1){
      x1 = x-1;
    }
    theta1 = dir==1?(theta+270)%360:(theta+90)%360;
    break;
  case 5:
    y1 = y-1;
    theta1 = (theta+180)%360;
    break;
  case 95:
    x1 = x+1;
    theta1 = (theta+180)%360;
    break;
  case 185:
    y1 = y+1;
    theta1 = (theta+180)%360;
    break;
  case 275:
    x1 = x-1;
    theta1 = (theta+180)%360;
    break;
  }
  pos[0]= x1;
  pos[1]= y1;
  pos[2]= theta1;
}

void sort(int open[100][7], int count){
  int i ,j,a;
  int temp[7];
  for(i=count-1;i>=0;i--){
    for(j=0;j<=i;j++){
      if(open[j][0]>open[i][0]){
	for(a=0;a<7;a++){
	  temp[a] = open[j][a];
	}
	for(a=0;a<7;a++){
	  open[j][a] = open[i][a];
	}
	for(a=0;a<7;a++){
	  open[i][a] = temp[a];
	}
      }
    }
  }
}

void pop(int open[100][7],int count, int next[7]){
  int a;
  for(a=0;a<7;a++){
	  next[a] = open[0][a];
  }
  int i;
  for(i=0;i<count-1;i++){
    for(a=0;a<7;a++){
	  open[i][a] = open[i+1][a];
    }
  }
}
int heruistic(x,y,goalx,goaly){
  x=goalx-x;
  if (x<0)
    x=x*(-1);
  y=goaly-y;
  if (y<0)
    y=y*(-1); 
  return x+y;
}

int A_star(int initx, int inity, int goalx, int goaly, int gtheta, int cost[66], int path[66][60],int paircount){
  if(initx<0 || initx>24 || inity<0 || inity>28 || map[initx][inity]==0) return -1;
  if(goalx<0 || goalx>24 || goaly<0 || goaly>28 || map[goalx][goaly]==0) return -1;
  int  closed[25][29];
  int i,j,k;
  for(i=0; i<25; i++){
    for(j=0;j<29;j++){
      closed[i][j]=0;
    }
  }

  int  action[25][29];
  for(i=0; i<25; i++){
    for(j=0;j<29;j++){
      action[i][j]=-1;
    }
  }

  int open[100][7]; //100?
  int x,y;
  int g=0,h=0;
  int f = g+h;
  int theta;
  open[0][0] =f;
  open[0][1] =g;
  open[0][2]= h;
  open[0][3] =initx;
  open[0][4] =inity;
  open[0][5] =gtheta;
  open[0][6] = 0;
  int opencount  = 1;//count of elements in open
  int found=0,resign=0;//goal found, resigned with goal not found
  int costs[5]={1,1,4,4,7};
  int next[6];//poped element from open which has least cost
  int c=0,x1,y1,g1,f1,h1,theta1; //constants for while
  int pos[3];
  while(found!=1||resign!=1){
    if(opencount==0){
      resign=1;
      break;
    }
    else{
      sort(open,opencount);  
      pop(open,opencount,next);
      opencount--;
      x = next[3];
      y = next[4];
      theta = next[5];
      g = next[1];
      action[x][y]= next[6];
      if((x == goalx) && (y == goaly)){
	found=1;
	gtheta = theta;
	break;
      }else{
	for(k =1; k<5;k++){
	  pos[0] = x;
	  pos[1] = y;
	  pos[2] = theta;
	  move(pos,k,1);
	  x1 = pos[0];
	  y1 = pos[1];
	  theta1 = pos[2];
	  if(x1>=0 && x1<=24 && y1>=0 && y1<=28 ){
	    if(closed[x1][y1]==0 && map[x1][y1]==0){
	      g1 = g+ costs[k-1];
	      h1 = heruistic(x1,y1,goalx,goaly);
	      f1 = h1+g1;
	      open[opencount][0] =f1;
	      open[opencount][1] =g1;
	      open[opencount][2]= h1;
	      open[opencount][3] =x1;
	      open[opencount][4] =y1;
	      open[opencount][5] =theta1;
	      open[opencount][6] = k;
	      //     printf("%d,%d,%d,%d,%d,%d,%d\n",f1,g1,h1,x1,y1,theta1,k);
	      opencount++;
	    }
	  }		
	}
	closed[x][y]=1;
      }
    }
  }
  pos[0] = goalx;
  pos[1] = goaly;
  pos[2] = gtheta;
  cost[paircount] = costs[action[pos[0]][pos[1]]];
  path[paircount][0] = action[pos[0]][pos[1]];
  c=1;// counter for path elements
  while(pos[0]!=initx || pos[1]!=inity){
    move(pos,action[pos[0]][pos[1]],-1);
    //    printf("%d,%d,%d\n",pos[0],pos[1],pos[2]);
    path[paircount][c] = action[pos[0]][pos[1]];
    cost[paircount] += costs[action[pos[0]][pos[1]]-1];
    c++;
    }
  c--;
  int temp=0;
  for(i=0;i<=c/2;i++){
    temp = path[paircount][i];
    path[paircount][i] = path[paircount][c-i-1];
    path[paircount][c-i-1] = temp;
  }   
  return 0;
}

#endif /* ASTAR_H_ */
