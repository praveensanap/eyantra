
/*
 * fertilizer.c
 *
 * Created: 19-02-2014 10:30:30
 *  
 Author: Praveen
 Author: Shreyas
 Author: Sairaj
 Author: Harshit
********************************************************************************/
#define F_CPU 14745600
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <math.h> //included to support power function
#include "initdevices.h"
#include "devices.h"
//#include "constants.h"
//#include "Astar.h"
//#include "IncFile2.h"
#include "lcd.h"

void port_init (void)
{
	adc_pin_config();
	lcd_port_config();
  buzzer_pin_config();
  interrupt_switch_config();
  LED_bargraph_config();
  MOSFET_switch_config();	
  motion_pin_config();
  servo1_pin_config();
  left_position_encoder_interrupt_init();
  right_position_encoder_interrupt_init();
}


void init_devices (void)
{
  cli(); //Clears the global interrupts
  port_init();
  timer1_init();
  adc_init();
  timer5_init();
  sei(); //Enables the global interrupts
}

//Main Function
float X =0,Y =0 , dx = 0.04931726, dy = 2.719403783;
int L,R;

ISR(INT5_vect)
{
	ShaftCountRight++;  //increment right shaft position count
	X=X-dx;
	Y=Y+dy;
	
}

//ISR for left position encoder
ISR(INT4_vect)
{
	ShaftCountLeft++;  //increment left shaft position count
	X=X+dx;
	Y=Y+dy;
}

int main(void)	
{
	init_devices();
	lcd_set_4bit();
	lcd_init();
	_delay_ms(2000);
	int i;
	while(Y<400){
		int x = (int)X;
		int y = (int)Y;
		lcd_print(1,0,x,3);
		lcd_print(1,5,y,3);
		velocity(255,255);
		forward();
	}
	int x = (int)X;
	int y = (int)Y;
	lcd_print(1,0,x,3);
	lcd_print(1,5,y,3);
	stop();
_delay_ms(1000);


	/*
  init_devices();
  init_devices();
  lcd_set_4bit();
  lcd_init();
  forward1(150);
  right1();
  forward1(500);
  turnleftdrop();
  right1();
  buzzer_on();
  _delay_ms(30);
  buzzer_off();
  return 0;*/
}
